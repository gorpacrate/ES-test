var webpack = require('webpack');

module.exports = {
	entry: [
		'webpack-dev-server/client?http://localhost:8080',
		'webpack/hot/only-dev-server',
		"./scripts/app.js"
	],
	output: {
		path: __dirname + '/build',
		filename: "bundle.js"
	},
	module: {
		loaders: [
			{ test: /\.js?$/, loaders: ['react-hot-loader', 'babel-loader', 'component-css?ext=styl'], exclude: /node_modules/ },
			{ test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader' }
		]
	},
	plugins: [
		new webpack.NoErrorsPlugin()
	]
};
