import React from 'react';
import {Route} from 'react-router';
import App from './components/Root.js';

import NewsFeed from './components/NewsFeed.js';
import NewsFeedArticle from './components/NewsFeedArticle.js';
import About from './components/About.js';

let routes = (
  <Route name="app" path="/" handler={App}>
    <Route name="news-feed" path="/" handler={NewsFeed}>
      <Route name="news-feed-article" path="/article/:articleId" handler={NewsFeedArticle}/>
    </Route>
    <Route name="about" path="/about" handler={About}></Route>
  </Route>
);

export default routes;
