import React from 'react';
import Router from 'react-router';
import _ from 'lodash';

export default class NewsFeedArticle extends React.Component{
	render() {
    var article = _.find(this.props.news, {id: this.context.router.getCurrentParams().articleId*1});

		if(!article){
			return null;
		}

		function createMarkup(htmlString) {
			return {__html: article.content};
			// тут по как-то untaint'ить данные надо
		};

		return (
			<div className="article">
				<h2>{article.title}</h2>
				<div className="article-content" dangerouslySetInnerHTML={createMarkup()}></div>
			</div>
		);
	}
}

NewsFeedArticle.contextTypes = {
  router: React.PropTypes.func
};
