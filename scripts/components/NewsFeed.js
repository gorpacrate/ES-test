import React from 'react';
import { Link, RouteHandler } from 'react-router';
import httpGet from '../helpers/http.js';

export default class NewsFeed extends React.Component {
	constructor(props) {
		super(props);
		this.state = {news: []};
	}
	componentDidMount(){
		var self = this;
		httpGet('/assets/news.json', function(data){
			self.setState({ news: JSON.parse(data) });
		});
		// тут кеширование бы, чтобы каждый раз не скачивалось
	}
	render() {
		return (
			<div className="wrapper">
				<div className="articles-list">
					{this.state.news.map(function(article) {
						return (
							<div className="article-link-wrapper">
								<Link to="news-feed-article" params={{articleId: article.id}} >{article.title}</Link>
							</div>
						)
					})}
				</div>
				<RouteHandler news={this.state.news}/>
			</div>
		);
	}
}
