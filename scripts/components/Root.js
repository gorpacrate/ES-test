import React from 'react';
import { Link, RouteHandler } from 'react-router';



export default class App extends React.Component {
  render() {
    return (
      <div className="app">
        <nav>
          <Link to="news-feed">News feed</Link>
          <Link to="about">About us</Link>
        </nav>
        <div className="content">
          <RouteHandler/>
        </div>
      </div>
    );
  }
}

